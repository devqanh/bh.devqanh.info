<?php global $smof_data;
/**
 * The template used for displaying content of product. use get_template_part
 *
 * @package author KanG
 * @subpackage KanG
 * @since KanG 1.0
 */
?>
	<?php
	/* Get options of posts */
	$price = get_field('product_price');
	$old_price = get_field('kproduct_oldprice');
     $desc = get_field('kproduct_desc');  
  	$kproduct_kt = get_field('kproduct_kt');
  	$kproduct_des = get_field('kproduct_desc'); 
	if(!isset($i)) { $i = 1; } else { $i = $i; }
	
	$delay = '';
	if( $i%4 == 0 ){
		$delay = ' data-delay="250"';
	} elseif( $i%4 == 2 ){
		$delay = ' data-delay="250"';
	} elseif( $i%4 == 3 ){
		$delay = ' data-delay="250"';
	} else {
		$delay = ' data-delay="250"';
	}
	
	?>
		<div class="k-product come-on"<?php echo $delay; ?>>
<a class="frame_view" href="<?php the_permalink(); ?>">
<?php if($kproduct_des){ ?>
                                         
                                              <?php echo $kproduct_des; ?>
                                            
                                          <?php } ?></a>
			<a class="thumbnail" href="<?php the_permalink(); ?>">
			
			
				<?php 
				if( has_post_thumbnail() ) {
					the_post_thumbnail('thumb_400x400');
					
				} else {
					echo '<img src="'.THEME_URL.'/images/thumb-200x200.jpg" alt="'.get_the_title().'" />';
				}
				?>
				
										  
										  
                                         <?php if($old_price){ ?>
                                      <div class="sale-off">
                                       <span><?php if(isset($price) && !empty($price) && isset($old_price) && $old_price > $price){
 echo   '-' . (100  - ceil(($price/ $old_price) * 100)) . '%';
}
 ?></span>
                                      </div> 
                                <?php } ?>
			

					</a>
 

			<h3 class="title-product"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<div class = "price-home">
			<?php if(isset($old_price) && !empty($old_price) && ( $old_price > $price )): ?>
					<p class="old_price"><del><?php _e('','kang'); echo number_format($old_price, 0, "",".").'<span class="cur cur-layout2"> &#8363;</span>'; ?></del></p>
					<?php endif; ?>
			<?php if( isset($price) && !empty($price) ){ ?>
			<p class="price"><?php echo number_format($price, 0, "", "."); ?><span class="cur"> &#8363;</span></p>
			<?php } else { ?>
			<p class="price no-price"><?php _e('Liên hệ','kang'); ?></p>
			<?php } ?>
			</div>
		</div>
	
	<?php $i++; ?>