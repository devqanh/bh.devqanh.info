<?php global $smof_data;
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 *
 * @package author KanG
 * @subpackage KanG
 * @since KanG 1.0
 */

get_header();
?>
<nav id="site-navigation" class="main-navigation" role="navigation">
			
				<?php wp_nav_menu( array( 'theme_location' => 'menu-sidebar', 'menu_class' => 'nav-menu' ) ); ?>
			
		</nav>
	
	
	
 <div class="main-banner">
<?php
$slider = $smof_data['gallery-slider-home'];
if(!empty($slider)){
    ?>
    <div class="flexslider home-flexslider">
        <ul class="slides">
            <?php foreach($slider as $slide) {
                $image = $slide['url'];
                $link = $slide['link'];
                if(!empty($link) && isset($link)){ $open_slider = '<a href="'.esc_url(trim($link)).'">'; $close_slider = '</a>'; } else { $open_slider = ''; $close_slider = ''; }
                ?>
                <li><?php echo $open_slider; ?><img src="<?php echo $image; ?>" alt="<?php _e('Image home slider','kang'); ?>" /><?php echo $close_slider; ?></li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>


    <div class="h-r">
        <ul class="come-on" data-delay="150">
 <?php dynamic_sidebar('slideleft'); ?> 
 </ul>
    </div>
</div>

    <div id="primary" class="site-content">
        <div class="content-right">
		
		
		<?php     
		$product_cat1 = $smof_data['product-cat-home1'];
		$product_cat2 = $smof_data['product-cat-home2'];
		$product_cat3 = $smof_data['product-cat-home3'];
		$product_cat4 = $smof_data['product-cat-home4'];
		$product_cat5 = $smof_data['product-cat-home5'];
		$product_cat6 = $smof_data['product-cat-home6'];
		$product_cat7 = $smof_data['product-cat-home7'];
		$product_cat8 = $smof_data['product-cat-home8'];
		$product_cat9 = $smof_data['product-cat-home9'];
		$product_cat10 = $smof_data['product-cat-home10'];
        $product_number = $smof_data['product-cat-numbers'];
		?>	 
			
			<?php if($product_cat1){ ?>	 
		<div class = "product-home">
	
<?php
         
            
			
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat1 = str_replace(' ', '', $product_cat1);
            $product_cat1 = explode(',', $product_cat1);
            if(!empty($product_cat1)):
                foreach( $product_cat1 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory1')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
			
					
			<div class="bannerhome4">
<ul class="come-on" data-delay="0">
 <div class="bannerqc4"><?php dynamic_sidebar('bannerbottomcategory1'); ?>  </div> 
 </ul>
</div>
	</div>	  
	
	
	
	      <?php } ?>
			<?php if($product_cat2){ ?>	
		  
		
		<div class = "product-home">
		
<?php
          
        
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat2 = str_replace(' ', '', $product_cat2);
            $product_cat2 = explode(',', $product_cat2);
            if(!empty($product_cat2)):
                foreach( $product_cat2 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory2')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
			
				 

   
	</div>	 
	      <?php } ?>
			<?php if($product_cat3){ ?>	
 		<div class = "product-home">
		
<?php
          
            
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat3 = str_replace(' ', '', $product_cat3);
            $product_cat3 = explode(',', $product_cat3);
            if(!empty($product_cat3)):
                foreach( $product_cat3 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory3')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>

	</div>	
		      <?php } ?>
			<?php if($product_cat4){ ?>
	
	
		<div class = "product-home">
		
<?php
          
          
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat4 = str_replace(' ', '', $product_cat4);
            $product_cat4 = explode(',', $product_cat4);
            if(!empty($product_cat4)):
                foreach( $product_cat4 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory4')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
			
				 
			<div class="bannerhome6">
<ul class="come-on" data-delay="0">
 <div class="bannerqc6"><?php dynamic_sidebar('bannerbottomcategory4'); ?>  </div> 
 </ul>
</div>
   
	</div>	
	
		      <?php } ?>
	<?php if($product_cat5){ ?>	 
		<div class = "product-home">
	
<?php
         
            
			
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat5 = str_replace(' ', '', $product_cat5);
            $product_cat5 = explode(',', $product_cat5);
            if(!empty($product_cat1)):
                foreach( $product_cat5 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory5')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			

	</div>	  
	
	
	
	      <?php } ?>					
			<?php if($product_cat6){ ?>
	
		<div class = "product-home">
		
<?php
          
            
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat6 = str_replace(' ', '', $product_cat6);
            $product_cat6 = explode(',', $product_cat6);
            if(!empty($product_cat6)):
                foreach( $product_cat6 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory6')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>

   
	</div>	
	      <?php } ?>
		<?php if($product_cat7){ ?>
		<div class = "product-home">
		
<?php
          
           
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat7 = str_replace(' ', '', $product_cat7);
            $product_cat7 = explode(',', $product_cat);
            if(!empty($product_cat7)):
                foreach( $product_cat7 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory7')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
   
	</div>	
	      <?php } ?>
	<?php if($product_cat8){ ?>
		<div class = "product-home">
		
<?php
          
           
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat8 = str_replace(' ', '', $product_cat8);
            $product_cat8 = explode(',', $product_cat8);
            if(!empty($product_cat8)):
                foreach( $product_cat8 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory8')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
   
	</div>	
	      <?php } ?>
		<?php if($product_cat9){ ?>
	
		<div class = "product-home">
		
<?php
          
            
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat9 = str_replace(' ', '', $product_cat9);
            $product_cat9 = explode(',', $product_cat9);
            if(!empty($product_cat9)):
                foreach( $product_cat9 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>



<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';









// chèn ảnh bannrer

if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('bannerleftcategory9')): endif;

// end ảnh 




 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
   
	</div>	

	      <?php } ?>
	
	<?php if($product_cat10){ ?>
	    
		<div class = "product-home">
		
<?php
 
 
 
            if(empty($product_number)): $product_number = '4'; endif;
            $product_cat10 = str_replace(' ', '', $product_cat10);
            $product_cat10 = explode(',', $product_cat10);
            if(!empty($product_cat10)):
                foreach( $product_cat10 as $product ){
                    $args = array(
                        'post_type'	=>	'post',
                        'posts_per_page'	=>	$product_number,
						
                        'tax_query'	=> array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $product,
                            ),
                        ),
                    );
                    $query = new WP_Query($args);
                    if($query->have_posts()):
			
                        $product_oj = get_category_by_slug($product);
                        $product_id = $product_oj->term_id;
                        echo '<div class="feature-products">';
                        echo '<h2 class="come-on" data-delay="0">';
                        echo '<div class="tile-cat">';
                        echo '<a class="cat-name" href="'.get_category_link($product_id).'">'.$product_oj->name.'</a>';

$sub_categories = get_categories( 'hide_empty=0&child_of=' . $product_id . '&parent=' . $product_id );

if ( count( $sub_categories ) ) {
    foreach ( $sub_categories as $sub_category ) {
        ?>
		
        <a class="subcate" href="<?php echo get_category_link( $sub_category->term_id ) ?>"
           title="<?php echo $sub_category->cat_name; ?>"><?php echo $sub_category->cat_name; ?></a>
        <?php
		
    }
}
echo'</div>
<a class="viewall" href="'.get_category_link($product_id).'">'.__("Xem tất cả ","kang").'</a></h2>';
// chèn ảnh bannrer
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('hinhquangcao10')): endif;
// end ảnh 
 		                        echo '<div class="all-products-home">';
						while($query->have_posts()): $query->the_post();
                            include( locate_template( 'content-product.php' ) );
                        endwhile; wp_reset_postdata();
						echo '</div>';
						echo '</div>';
				                endif;
                }
            endif;

            ?>
			
	</div>	
 <?php } ?>

 
 
 




		<div class = "home-new">  	


			<div class = "new_post_index">

<h2 style="text-align: center; text-align: LEFT;
    BACKGROUND: #fffff4;
    BORDER-LEFT: 10PX SOLID #ff9600;
    PADDING-LEFT: 10PX;
    LINE-HEIGHT: 37PX;
    BORDER-BOTTOM: 1PX SOLID #53ac19;
    COLOR: #53ac19;
    FONT-WEIGHT: BOLD;">TIN TỨC</h2>
                <?php 
				$args = array(
						'showposts' => $numbers,
						'post_type' => 'bai-viet',
						'posts_per_page'	=> 4,
						
					);
				if(!empty($cat)){
					$args['tax_query'] = array(
											array(
												'taxonomy' => 'danh-muc',
												'field'    => 'id',
												'terms'    => $cat,
											),
										);
				}
				
				$news = new WP_Query($args);
				 ?>
				<?php while($news->have_posts()): $news->the_post();?>
				 
				 
			<div class = "new_post_1">
				<li class="kang-post come-on"<?php echo $delay; ?>>
		<div class="thumbnail_new">
			<a href="<?php the_permalink();?>">
				<?php if(has_post_thumbnail()) { the_post_thumbnail('thumb_300x240'); } else { echo '<img src="'.THEME_URL.'images/thumb-300x240.jpg" alt="'.get_the_title().'">'; }?>
			</a>
		</div>
		<div class="detail-post-new">
			<h3 class="title-post-new"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			<div class = "info-bai-viet">
			
		<?php kang_entry_meta() ?>      
			<a class="read-more" href="<?php the_permalink();?>"><?php _e('Xem chi tiết','kang'); ?></a>
			</div>
			</div>
	</li>
	</div>


<?php  endwhile ; wp_reset_query() ;?>
	
</div>


<div style="
    background: #fffff4;
    border: 1px dotted #ff7703;
    padding: 2em;
    margin-top: 10px;
    display: inline-block;
">
<h1 style="text-align: center;"><span style="font-weight: 400;">Máy lọc nước Kangaroo - Máy lọc RO thế hệ mới</span></h1>
<span style="font-weight: 400;">Máy lọc nước Kangaroo là một sản phẩm ứng dụng nguyên lý “Quả thận bên ngoài cơ thể” để giúp cho nguồn nước đầu ra của gia đình bạn được đảm bảo. Nhiều khách hàng vẫn băn khoăn không biết máy lọc nước Kangaroo có những tính năng gì, máy lọc nước có những ưu điểm vượt trội như thế nào? Được cấu tạo ra sao? Hãy tham khảo bài viết này ngay nhé.</span>
<ol>
	<li style="font-weight: 400;">
<h2><span style="font-weight: 400;">Giới thiệu về  công nghệ RO Kangaroo</span></h2>
</li>
</ol>
<span style="font-weight: 400;"><strong>Máy lọc nước Kangaroo</strong> được chứng nhận có khả năng loại bỏ những tạp chất nguy hiểm ảnh hưởng tới sức khỏe của con người như: thuốc trừ sâu, kim loại nặng,vi rút, vi sinh vật…</span>

<span style="font-weight: 400;"><strong>Máy lọc nước Kangaroo ứng</strong> dụng Công nghệ RO (công nghệ thẩm thấu ngược) là công nghệ lọc nước tiên tiến và triệt để nhất hiện nay. Bởi lẽ, các khe hở màng lọc RO có kích cỡ 0,001 micromet, giống như cơ chế hoạt động của thận người sẽ cho ra sản phẩm nước hoàn toàn nguyên chất.</span>

<span style="font-weight: 400;">Nguồn nước đầu vào của gia đình bạn sau khi đi qua các màng lọc có thể cho ra nguồn nước tinh khiết, sạch tiệt trùng có thể uống ngay được mà không cần phải đun sôi.</span>

<span style="font-weight: 400;">Công nghệ thẩm thấu ngược hoạt động theo nguyên lý chênh lệch áp suất giữa đầu ra và đầu vào của hệ thống lõi lọc. Từ đó, nước sẽ được đưa qua nhiều cấp lọc khác nhau từ lọc thô đến lọc tinh khiết giúp lọc các cặn bẩn và vi khuẩn từ to tới nhỏ. </span>

<span style="font-weight: 400;">Đặc biệt, máy lọc nước Kangarooo có hệ thống lọc nhiều cấp theo nhu cầu và nguồn nước sử dụng :lõi lọc công nghệ Nano Silver, lõi lọc bóng gốm,lõi lọc Alkaline, lõi lọc tạo  khoáng, tạo vị, ...Nhứng lõi lọc này giúp cân bằng độ PH, bổ sung khoáng chất cần thiết cho cơ thể và làm giàu lượng Oxy trong nước.</span>

<span style="font-weight: 400;">Máy lọc nước Kangaroo sẽ giúp bảo vệ sức khỏe của gia đình bạn một cách tốt nhất. Nhờ công nghệ lọc hiện đại này, sẽ hạn chế tối đa các tác nhận gây bệnh qua nguồn nước, cung cấp những khoáng chất thiết yếu cho con người.</span>
<ol start="2">
	
	</div>


        <!-- .content-right -->

    </div><!-- #primary -->
</div>

<?php get_footer(); ?>
