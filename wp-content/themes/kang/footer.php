<?php global $smof_data, $post;
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package author KanG
 * @subpackage KanG
 * @since KanG 1.0
 */
 /* Get data footer in theme options */
 
?>
<?php	
			$hotline = $smof_data['header-hotline'];
			?>
		</div><!-- #main .wrapper -->
	</div><!-- #page -->
	<footer id="colophon" role="contentinfo">
<div class="widget-top col-<?php echo $top_widget; ?>">
				<?php dynamic_sidebar('footer-top'); ?>
			
		</div>	
			


<div class="wf-top">
        <ul class="l">
            <li class="wf-top-sphh"><span>Sản phẩm, hàng hóa</span> <span>Chính hãng đa dạng phong
                phú.</span></li>
            <li class="wf-top-kmkn"><span>Luôn luôn giá rẻ &amp;</span> <span>khuyến mại không ngừng.</span></li>
            <li class="wf-top-dv"><span>Dịch vụ</span> <span>Chăm sóc khách hàng uy tín.</span></li>
            <li class="wf-top-tvbh"><span>Tư vấn bán hàng</span> <b><a href="tel:<?php echo $hotline; ?>"><?php echo $hotline; ?></a></b></li>
        </ul>
    </div>






			
			<div class = "wf-top1">
<ul class="l">
<li class="new-letter">
<span class = "dkmail"> ĐĂNG KÝ NHẬN TIN KHUYẾN MẠI </span>
<?php echo do_shortcode('[contact-form-7 id="2768" title="Đăng ký"]') ?>
</li>
<?php 
			/* List social */
			$facebook = $smof_data['social-facebook'];
			$google = $smof_data['social-google'];
			$youtube = $smof_data['social-youtube'];
			$twitter = $smof_data['social-twitter'];
			$pinterest = $smof_data['social-pinterest'];
	
			if( !empty($facebook) || !empty($google) || !empty($youtube) || !empty($pinterest) || !empty($twitter)  ):
			?>
				<div class="social-footer">
					<ul class="social-media">
			
						<?php if(!empty($facebook)): ?>
						<li class="facebook"><a target="_blank" href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a></li>
						<?php endif;?>
						<?php if(!empty($google)): ?>
						<li class="google"><a target="_blank" href="<?php echo $google; ?>"><i class="fa fa-google-plus"></i></a></li>
						<?php endif;?>
						<?php if(!empty($youtube)): ?>
						<li class="youtube"><a target="_blank" href="<?php echo $youtube; ?>"><i class="fa fa-youtube"></i></a></li>
						<?php endif;?>
						<?php if(!empty($twitter)): ?>
						<li class="twitter"><a target="_blank" href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
						<?php endif;?>
						
						<li class="pinterest"><a target="_blank" href="<?php echo $pinterest; ?>"><i class="fa fa-pinterest"></i></a></li>
						
						
					</ul>
				</div>
			<?php endif; // End check if all social is empty ?>
				
</ul>
	</div>

			
			
			
			
			
			
			
			
		<div class="container footer-top">
			<?php 
			$sidebar = wp_get_sidebars_widgets();
			$top_widget = count($sidebar['footer-top']);
			if(!empty($top_widget)){
			?>
			
			<?php
			}
			?>
		
			<?php 
			$bottom_widget = count($sidebar['footer-bottom']);
			if(!empty($bottom_widget)){
			?>
			<div class="widget-bottom col-<?php echo $bottom_widget; ?>">
				<?php dynamic_sidebar('footer-bottom'); ?>
			</div>
			<?php
			}
			?>
		</div>
	</footer><!-- #colophon -->
	
	<div class="copyright">
		<div class="container">
			
			<div class="footer-copyright"><?php echo $smof_data['footer-copyright'];?></div>
		</div>
	</div>
	
	<?php $post_type = get_post_type(); ?>
	<?php if(is_single() && $post_type == 'post'): ?>
		<?php 
		/* Get data of current product */
		$postID = get_the_ID(); 
		$title = single_post_title('', false);
		$abovePopup = $smof_data['above-popup'];
		$bellowPopup = $smof_data['bellow-popup'];
		$success = $smof_data['message-success'];
		$failed = $smof_data['message-failed'];
		

			
		if ( has_post_thumbnail( $postID ) ) {
			$image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $postID ), 'thumb_400x400' );
			$imageURL = $image_array[0];
		} else {
			$imageURL = get_template_directory_uri() . '/images/thumb-400x400.jpg';
		}
		?>
			<div class="kang-form-order" id="order-form">
			<?php echo do_shortcode('[contact-form-7 id="2778" title="Thông tin đặt hàng"]'); ?>
	
		</div>
		<div class="order-success">
			<h2 class="title-form"><?php _e('Đặt hàng thành công!','kang'); ?><a class="close-order"><i class="fa fa-close"></i></a></h2>
			<div class="content-success"><?php echo $success; ?><i class="fa fa-check-circle-o"></i></div>
		</div>
		
		<div class="order-failed">
			<h2 class="title-form"><?php _e('Đặt hàng thất bại!','kang'); ?><a class="close-order"><i class="fa fa-close"></i></a></h2>
			<div class="content-success"><?php echo $failed; ?><i class="fa fa-warning"></i></div>
		</div>
		
	<?php endif; //End check if is single product ?>
	
	<?php if(empty($banner_right)): ?>
	<a class="k-gototop" href="#"><i class="fa fa-angle-up"></i></a><!--.k-gototop-->
	<?php endif; ?>
	
<?php wp_footer(); ?>
<script language="javascript" src="http://mayloc.ninhbinhweb.com/wp-content/themes/kang/js/jquery-1.9.1.min_.js"></script>

<script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
<script src="http://mayloc.ninhbinhweb.com/wp-content/themes/kang/js/bootstrap.min.js"></script>

<script>
$(document).ready(function () {
    //Hover Menu in Header
    jQuery('ul.nav li.dropdown').hover(function () {
        jQuery(this).find('.mega-dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        jQuery('.darkness').stop(true, true).fadeIn();
    }, function () {
        jQuery(this).find('.mega-dropdown-menu').stop(true, true).delay(200).fadeOut(200);
         jQuery('.darkness').stop(true, true).delay(200).fadeOut();
    });
});
    	
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'vi'}
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="http://mayloc.ninhbinhweb.com/wp-content/themes/kang/js/jquery.sticky-kit.min.js"></script>
<script>
$(".order_right").stick_in_parent();
</script>
<script>
$(".navbar-default").stick_in_parent();
</script>






</body>


</html>