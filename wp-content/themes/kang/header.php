<?php global $smof_data;
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package author KanG
 * @subpackage KanG
 * @since KanG 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta name="google-site-verification" content="aecfhWVbLx6RGs8PwVEpRkX39S9YpfEwvsKOPyOsNcE" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
<?php if(is_home() || is_front_page()): ?>
<?php endif; /* End check if is home page or front page */ ?>
<link rel="profile" href="http://gmpg.org/xfn/11" rel="friend met" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php if( isset ( $smof_data['favicon'] ) &&  $smof_data['favicon'] != '' ) { ?>
<link rel="shortcut icon" href="<?php echo $smof_data['favicon']; ?>" />
<link rel="stylesheet" type="text/css" href="http://mayloc.ninhbinhweb.com/wp-content/themes/kang/bootstrap/bootstrap.css" />

<?php } ?>
<?php // Loads HTML5 JavaScript file to add support for HTML5 ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66372632-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<div class="container">
			<div class="header-left">
				<div id="logo">
					<?php /* Check logo */ $logo = ($smof_data['logo']) ?  $smof_data['logo'] : THEME_URL.'/images/logo.png'; ?>
					<a href="<?php echo HOME_URL; ?>"><img src="<?php echo $logo; ?>" alt="<?php _e('Logo','kang'); ?>"/></a>
				</div>
			</div>
                        <!-- .kang-head -->
			<div class="header-center">
<?php 
$htitle = $smof_data['header-title'];
$hotline = $smof_data['header-hotline'];
?>
<?php if (is_home()) echo('
<div style="text-align: center;"><h1>
<b><span style="color: #53AC19; font-size: 17px;text-transform: uppercase;">'.$htitle.'</span></b></h1></div>
') ?>

<?php if (have_posts()) echo('
<div class="header-center-title" style="text-align: center;"><h2>
<b><span style="color: #3DAD49; font-size: 17px;text-transform: uppercase;">'.$htitle.'</span></b></h2></div>
') ?>

				<?php get_search_form(); ?>

			<div class = "gsearch">
			<span>Gợi ý tìm kiếm :</span> <a href="http://mayloc.ninhbinhweb.com/may-loc-nuoc-gia-dinh">Máy lọc nước</a>...
			</div>
 			</div>
			<!--end header ringht--->


<div class="header-info">

            <div class="tel"><span>Hotline mua hàng: <a href="tel:<?php echo $hotline; ?>"><?php echo $hotline; ?></a></span></div>
            <div class="text t1">
                <div class="l">
                    <a rel="nofollow" href="#">&nbsp;</a></div>
                <div class="r">
                    <a rel="nofollow" href="#">
                        <b>Giao Hàng</b><br>
                        Trên toàn quốc
                    </a>
                </div>
            </div>
            <div class="text t2">                
                <div class="l">
                    <a rel="nofollow" href="#">&nbsp;</a></div>
                <div class="r">
                    <a rel="nofollow" href="#">
                        <b><?php echo $hotline; ?></b><br>
                        Bảo hành chính hãng
                    </a>
                </div>
            </div>
            <div class="text t3">
                <div class="l">
                    <a rel="nofollow" href="#">&nbsp;</a></div>
                <div class="r">
                    <a rel="nofollow" href="#">
                        <b>Thanh toán</b><br>
                        Khi nhận hàng
                    </a>
                </div>
            </div>
          
            <div class="clear"></div>
        </div>
</header>

		</div>

<nav class="navbar navbar-default mega-nav">
        <div class="container">

        <div class="collapse navbar-collapse" id="MainMenu">
            <div class="collapse navbar-collapse" id="MainMenu">
<ul class="nav navbar-nav menu-list">
                <li class="dropdown list-category">
     	<a href="javascript:void(0);"class="dropdown-toggle" data-toggle="dropdown"
 role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-navicon"></i> DANH MỤC SẢN PHẨM <i class="fa fa-angle-down" aria-hidden="true"></i>     
                    </a>
<?php wp_nav_menu(array(
                    'theme_location'  => 'menu-sidebar',
                    'container'=> '0',
                    'container_class' => '0',
                    'container_id'=> '0',
                    'items_wrap' => '<ul class="dropdown-menu mega-dropdown-menu">%3$s</ul>' )
            );
            ?>
                </li>
<?php wp_nav_menu(array(
            'theme_location'  => 'menu-top',
            'container'=> '0', 
            'container_class' => '0', 
            'container_id'=> '0', 
            'items_wrap' => '<li>%3$s</li>' )
            ); 
          ?>

</ul>
     
   

 </div>

      </div>
	   </div>
    </nav>

       
       
   

<!-- #site-navigation -->
	<!-- #masthead -->
	<div class="beradcrum">
<?php the_breadcrumb(); ?>
</div>
    <?php
	 if (is_home()) echo('<style>.beradcrum {display:none!important;}

#primary > .content-right {
    width: 100%;
    display: inline-block;
    float: left;
    padding-right: 0px;
}
.header-center-title {
  display: none;
}
</style>');

 ?>
   
	<div id="main" class="wrapper<?php if(is_home() || is_front_page()): echo ' homepage'; endif; ?>">